// Function filters the original lyrics from a dubstep infested string
function songDecoder(song = '') {
    // Check if there's actually input passed for type safety
    if (song === '') {
        throw new Error('No input has been provided!');
    }

    // Split the lyrics with WUB value according to requirements
    const lyrics = song.split('WUB');
    // Filter out any empty array entries
    const filtered = lyrics.filter((value) => { if (value !== '') return true });
    // Join the filtered string to get back the original lyrics in a spaced string
    return filtered.join(' ');
}
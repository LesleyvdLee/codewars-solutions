// The problem's tests require this class to be present
export class G964 {
    // Problem initially doesn't come with types, defeating the purpose of using TypeScript
    // nbYear calculates the amount of years at takes for a towns population to reach a threshold
    public static nbYear(initialPopulation: number, percentGrowth: number, migration: number, surpassPopulation: number) {
        // Copy the initial population
        let population = initialPopulation;
        // Set amount of years emulated to 0
        let years = 0;
        // If the population isn't greater or equal to the population to surpass, emulate a year
        while (!(population >= surpassPopulation)) {
            // Calculate the population growth according to requirements
            population = population + (population * (percentGrowth / 100)) + migration;
            // Increment year
            years++;
        }
        // Return the amount of years it took to reach the threshold
        return years;
    }
}
// reverseWords accepts a sentence, and the words must be reversed while the sentence stays in the same order.
export function reverseWords(str: string): string {
    // Split the sentence into an array
    const arr = str.split(' ');
    // Use a regular for loop, since they're faster than a forEach loop (and in a lot of cases a map loop too)
    for (let i = 0; i < arr.length; i++) {
        // Split the word into an array of letters
        const word = arr[i].split('');
        // Reverse the array using a built in function
        arr[i] = word.reverse().join('');
    }
    // Return a joined sentence with spaces and reversed words
    return arr.join(' ');
}
#include <vector>

// Calculates average from vector with integers
double calcAverage(const std::vector<int>& values){
    // The total sum of all values
    double total = 0;
    // Adds all values to the sum
    for(size_t i = 0; i < values.size(); i++)
    {
            total += values[i];
    }
    // Returns the average by dividing by the vector length
    return total / values.size();
}
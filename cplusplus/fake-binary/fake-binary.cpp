#include <string>
#include <array>

// Input a string of numbers, returns binary for said string (not actual binary)
std::string fakeBin(std::string str){
    // Loop over every character in the string
    for(size_t i = 0; i < str.size(); i++) {
        // Read somewhere that when a char is a number and you subtract '0', you get an integer
        int number = str[i] - '0';
        // Replace with 0 if under 5, else with 1 (according to problem)
        if (number < 5) {
            str.replace(i, 1, "0");
        } else {
            str.replace(i, 1, "1");
        }
    }
    // Return the modified string
    return str;
}
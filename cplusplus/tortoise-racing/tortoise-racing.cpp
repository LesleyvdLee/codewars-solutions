#include <vector>

class Tortoise
{
public:
    static std::vector<int> race(int v1, int v2, int g) {
        // if v1 is larger or equal to v2, v2 can't catch up
        if (v1 >= v2) {
            return {-1, -1, -1};
        }

        // Get the speed v2 catches up by every hour (as a double to allow precise division)
        double catchupSpeed = v2 - v1;
        // Initialize variables
        int seconds = 0;
        int minutes = 0;
        int hours = 0;
        // Loop when the distance left is greater than 0
        while(g > 0){
            // If the distance left is smaller than the distance traveled in an hour, process minutes and seconds
            if (g < catchupSpeed) {
                // Get the total amount of seconds by dividing and multiplying by 3600 (amount of seconds in an hour)
                seconds += (g / catchupSpeed) * 3600;
                // If the amount of seconds is larger or equal to 60, loop and add minutes
                while(seconds >= 60){
                    seconds -= 60;
                    minutes += 1;
                }
                // Break the loop
                break;
            }
            // Add an hour and bring the distance closer
            hours += 1;
            g -= catchupSpeed;
        }

        // Return
        return {
            hours,
            minutes,
            seconds
        };
    }
};

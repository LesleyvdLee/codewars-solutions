#include <stddef.h>
#include <string.h>

size_t get_count(const char *s)
{
    // Set initial count to 0
    size_t count = 0;

    // Loop over the input string
    for(size_t i = 0; i < strlen(s); i++)
    {
        if (s[i] == 'a' || s[i] == 'e' || s[i] == 'u' || s[i] == 'o' || s[i] == 'i') {
            count++;
        }
    }
    
    // Return result
    return count;
}
# CodeWars solutions

This repository contains all of my own solutions to problems found at CodeWars.

## Preface

Solutions may be updated in the future if I find they can be improved or need reworking due to language changes.
Any solution may be used and modified without my discretion as long as the repository license is included.
This repository will not accept any issues or feedback related to any code found in here. For all intents and purposes this is a portfolio repository.

## Viewing solutions

The root of the repository contains directories which represent a language (like *csharp*).
In these directories you'll find more directories which represent the problems solved.
They may contain projects which need compiling, the solutions files, or whatever I see fit to reflect my solution.
If needed I'll provide a README with instructions on how to interact with the files.